#pragma once

#include "parameter.h"
#include "vector"

class VectorParameter : public Parameter
{

public:
    virtual std::tr1::shared_ptr< Parameter > zero() const;
    virtual std::tr1::shared_ptr< Parameter > operator/(const Parameter& pRight) const;
    virtual std::tr1::shared_ptr< Parameter > operator*(const Parameter& pRight) const;
    virtual std::tr1::shared_ptr< Parameter > operator-(const Parameter& pRight) const;
    virtual std::tr1::shared_ptr< Parameter > operator+(const Parameter& pRight) const;
    virtual std::string toString() const;
    virtual void write() const;
    virtual void read();
    inline void setAt(unsigned int pID, std::tr1::shared_ptr< Parameter > pValue)
	{
		if(mValues.size() < pID + 1)
			mValues.resize(pID + 1);
		mValues[pID] = pValue;
	}
    const std::tr1::shared_ptr<Parameter>& getAt(unsigned int pID)const {return mValues[pID];};
    unsigned getSize() const
	{
		return mValues.size();
	}
protected:
	std::vector<std::tr1::shared_ptr<Parameter> > mValues;
};


