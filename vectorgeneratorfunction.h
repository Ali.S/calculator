#ifndef VECTORGENERATORFUNCTION_H
#define VECTORGENERATORFUNCTION_H

#include "function.h"
#include <vector>

class VectorGeneratorFunction : public Function
{

public:
	VectorGeneratorFunction();
	VectorGeneratorFunction(std::tr1::shared_ptr<Function> pFirstArgument);
	virtual unsigned int priority();
    virtual void setArgument(unsigned int argumentID, std::tr1::shared_ptr< Function > pFunction);
    virtual unsigned int getArgumentNumber();
    virtual std::tr1::shared_ptr< Function > clone();
    virtual std::string toString();
    virtual std::tr1::shared_ptr< Function > derivation();
    virtual std::tr1::shared_ptr< Function > simplify();
    virtual std::tr1::shared_ptr< Parameter > evaluate();
protected:
	std::vector< std::tr1::shared_ptr<Function> > mArguments;
};

#endif // VECTORGENERATORFUNCTION_H
