#include "constantfunction.h"
#include <sstream>
#include <limits>

using namespace std;

ConstantFunction::ConstantFunction() :Function(), mValue(tr1::shared_ptr < Parameter >((Parameter*)NULL))
{

}

ConstantFunction::ConstantFunction(tr1::shared_ptr< Parameter > pValue): Function(), mValue(pValue)
{

}

ConstantFunction::~ConstantFunction()
{

}
unsigned int ConstantFunction::getArgumentNumber()
{
    return 0;
}

void ConstantFunction::setArgument(unsigned int argumentID, tr1::shared_ptr< Function > pFunction)
{

}

std::tr1::shared_ptr< Function > ConstantFunction::clone()
{
    return tr1::shared_ptr<Function>(new ConstantFunction(this->mValue));
}

std::string ConstantFunction::toString()
{
    return mValue->toString();
}

std::tr1::shared_ptr< Function > ConstantFunction::derivation()
{
    return tr1::shared_ptr < Function > (new ConstantFunction(tr1::shared_ptr<Parameter>(mValue->zero())));
}

std::tr1::shared_ptr< Function > ConstantFunction::simplify()
{
    return tr1::shared_ptr< Function > (this);
}

std::tr1::shared_ptr< Parameter > ConstantFunction::evaluate()
{
    return mValue;
}

unsigned int ConstantFunction::priority()
{
    return numeric_limits< unsigned int >::max();
}

