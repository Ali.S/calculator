#include "function.h"
#include "basicarithmeticoperator.h"
#include "customvariablefunction.h"
#include "constantfunction.h"
#include "numberparameter.h"
#include "vectorgeneratorfunction.h"
#include "customfunction.h"
//#include "customfunction.h"
#include <stack>
#include <queue>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <iostream>

using namespace std;

map<string,tr1::shared_ptr<Function> >* Function::mKnownFunctions;


Function::Function()
{
	
}

Function::~Function()
{

}

tr1::shared_ptr<Function> Function::fromString(const std::string& data)
{
	unsigned pos = 0;
	return localFromString(data, pos);
}

tr1::shared_ptr<Function> Function::localFromString(const std::string& data, unsigned& pos)
{
	stack<pair<tr1::shared_ptr<Function>, unsigned> > sFunctions;
	vector<tr1::shared_ptr<Function> > vFunctions;
	ArgumentType argType;
	string temp;
	bool begining = true;
	for(;pos<data.length();pos++)
	{
		localSkipWhiteSpace(data,pos);
		if (pos>=data.length())
			goto end;
		switch (data[pos])
		{
			case '(' : pos ++;localInsert(sFunctions,vFunctions,localVectorGenerator(data,pos),numeric_limits< unsigned int >::max()); break;
			case ')' : goto end; break;
			case ',' : goto end; break;
			case '+' : localInsert(sFunctions,vFunctions,tr1::shared_ptr<Function>(new BasicArithmeticOperator('+'))); begining = true; break;
			case '-' : 
				if(begining)
					localInsert(sFunctions,vFunctions,tr1::shared_ptr<Function>(new BasicArithmeticOperator('n')));
				else
					localInsert(sFunctions,vFunctions,tr1::shared_ptr<Function>(new BasicArithmeticOperator('-'))),begining = true;  
				break;
			case '*' : localInsert(sFunctions,vFunctions,tr1::shared_ptr<Function>(new BasicArithmeticOperator('*'))); begining = true;  break;
			case '/' : localInsert(sFunctions,vFunctions,tr1::shared_ptr<Function>(new BasicArithmeticOperator('/'))); begining = true;  break;
			default :
				temp = "";
				localParseString(data,pos,temp,argType);
				//cerr << "parse string , parse result =" << temp << "\n";
				if (argType == argNumber)
					localInsert(sFunctions,vFunctions,tr1::shared_ptr<Function>(new ConstantFunction(tr1::shared_ptr<Parameter>(new NumberParameter(boost::lexical_cast<float,std::string>(temp))))));
				else
					if (mKnownFunctions->find(temp) != mKnownFunctions->end())
					{
						std::tr1::shared_ptr<Function> func = mKnownFunctions->find(temp)->second->clone();
						pos++;
						localSkipWhiteSpace(data,pos);
						pos ++;
						localInsert(sFunctions,vFunctions,localCustomFunctionGenerator(data,pos,func),numeric_limits< int >::max() - 1);
					}
					else
						localInsert(sFunctions,vFunctions,tr1::shared_ptr<Function>(new CustomVariableFunction(temp)));
				begining = false;
		}
	}
	end:
	for (;!sFunctions.empty();sFunctions.pop())
	{
		vFunctions.push_back(sFunctions.top().first);
	}
	tr1::shared_ptr<Function> res;
	createFunctionTree(vFunctions);
	//cerr << "tree created with " << vFunctions.size() << " nodes.\n";
	//for (unsigned i = 0;i < vFunctions.size();i++)	
		//cerr << vFunctions[i]->toString() << "\n";	
	return vFunctions.back();
}

tr1::shared_ptr< Function > Function::localCustomFunctionGenerator(const std::string& data, unsigned int &pos,std::tr1::shared_ptr<Function> target)
{
	int argumentNumber = 1;
	if (data[pos] != ')')
		target->setArgument(0,localFromString(data,pos));
	for(;pos <data.length() && data[pos]==',';argumentNumber++)
	{
		pos++;
		target->setArgument(argumentNumber,localFromString(data,pos));
	}
	return target;
}

tr1::shared_ptr< Function > Function::localVectorGenerator(const std::string& data, unsigned int& pos)
{
	int argumentNumber = 1;
	tr1::shared_ptr<Function> ret;
	ret = localFromString(data,pos);
	if(data[pos] == ',')
	{
		ret = tr1::shared_ptr<Function>(new VectorGeneratorFunction(ret));
		for(;pos <data.length() && data[pos]==',';argumentNumber++)
		{
			pos++;
			ret->setArgument(argumentNumber,localFromString(data,pos));
		}
	}
	return ret;
}


void Function::createFunctionTree(vector< tr1::shared_ptr< Function > >& pVector)
{
	vector <bool> used;
	used.resize(pVector.size());
	unsigned argumentNumber;
	for(unsigned i=0;i<pVector.size();i++)
	{
		used[i] = false;
		argumentNumber = pVector[i]->getArgumentNumber();
		for(unsigned j=1; argumentNumber > 0;j++)
			if (used [i - j] == false)
			{
				pVector[i]->setArgument(argumentNumber-1,pVector[i-j]);
				used [i - j] = true;
				argumentNumber --;
			}
	}
}

void Function::localParseString(const std::string& data, unsigned int& pos, string& target, Function::ArgumentType& dataType)
{
	dataType = argNumber;
	//if (dataType == ArgumentType::argNumber)
	for(;pos<data.length();pos++)
		if (data[pos] <= '9' && data[pos] >= '0')
			target += data[pos];
		else
			break;
	if (data[pos] == '.')
	{
		target += data[pos];
		pos++;
		for(;pos<data.length();pos++)
			if (data[pos] <= '9' && data[pos] >= '0')
				target += data[pos];
			else
				break;
		if (data[pos] == 'e' || data[pos] == 'E')
		{
			target += data[pos];
			pos++;
			if (data[pos] == '+' || data[pos] == '-')
			{
				target += data[pos];
				pos++;
			}
			for(;pos<data.length();pos++)
				if (data[pos] <= '9' && data[pos] >= '0')
					target += data[pos];
				else
					break;
		}
		/*else
			dataType = ArgumentType::argUnknown;*/
	}
	//else
		//dataType = ArgumentType::argUnknown;
	for (;pos<data.length();pos++)
		if ((data[pos] >='0' && data[pos] <= '9') ||
			(data[pos] >='a' && data[pos] <= 'z') ||
			(data[pos] >='A' && data[pos] <= 'Z'))
		{
			dataType = argUnknown;
			target += data[pos];
		}
		else
			break;
	pos --;
}

void Function::localInsert(stack< pair< tr1::shared_ptr< Function >, unsigned> >& pStack, vector< tr1::shared_ptr< Function > >& pVector, tr1::shared_ptr< Function > pFunction)
{
	localInsert(pStack,pVector,pFunction,pFunction->priority());
}

void Function::localInsert(stack< pair< tr1::shared_ptr< Function >, unsigned> >& pStack, vector< tr1::shared_ptr< Function > >& pVector, tr1::shared_ptr< Function > pFunction, unsigned int pOverridePriority)
{
	for (;(!pStack.empty()) && pStack.top().second > pOverridePriority;pStack.pop())
		pVector.push_back(pStack.top().first);
	pStack.push(pair<tr1::shared_ptr< Function >, unsigned>(pFunction,pOverridePriority));	
}



void Function::localSkipWhiteSpace(const std::string& data, unsigned& pos)
{
	for(;pos<data.length();pos++)
		if(string("\n \r\t").find(data[pos]) == string::npos)
			return;
}
