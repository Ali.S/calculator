#include "numberparameter.h"
#include <iostream>
#include <limits>
#include <boost/lexical_cast.hpp>

using namespace std;

NumberParameter::NumberParameter() : mValue(0)
{

}

NumberParameter::NumberParameter(const NumberParameter& pOther) : mValue(pOther.mValue)
{

}

NumberParameter::NumberParameter(double pValue): mValue(pValue)
{

}

NumberParameter::~NumberParameter()
{

}

std::tr1::shared_ptr<Parameter> NumberParameter::operator/(const Parameter& pRight) const
{
    std::tr1::shared_ptr<NumberParameter> temp;
    if (dynamic_cast<const NumberParameter*>(&pRight))
        temp = std::tr1::shared_ptr<NumberParameter>(new NumberParameter(this->mValue / static_cast<const NumberParameter*>(&pRight)->mValue));
    return temp;
}

std::tr1::shared_ptr<Parameter> NumberParameter::operator*(const Parameter& pRight) const
{
    std::tr1::shared_ptr<Parameter> temp;
    if (dynamic_cast<const NumberParameter*>(&pRight))
        temp = std::tr1::shared_ptr<NumberParameter>(new NumberParameter(this->mValue * static_cast<const NumberParameter*>(&pRight)->mValue));
	else
		temp = pRight * *this;
    return temp;
}

std::tr1::shared_ptr< Parameter > NumberParameter::operator-(const Parameter& pRight) const
{
    std::tr1::shared_ptr<Parameter> temp;
    if (dynamic_cast<const NumberParameter*>(&pRight))
        temp = std::tr1::shared_ptr<NumberParameter>(new NumberParameter(this->mValue - static_cast<const NumberParameter*>(&pRight)->mValue));
    else
		temp = pRight - *this;
    return temp;
}

std::tr1::shared_ptr<Parameter> NumberParameter::operator+(const Parameter& pRight) const
{
    std::tr1::shared_ptr<Parameter> temp;
    if (dynamic_cast<const NumberParameter*>(&pRight))
        temp = std::tr1::shared_ptr<NumberParameter>(new NumberParameter(this->mValue + static_cast<const NumberParameter*>(&pRight)->mValue));
	else
		temp = pRight + *this;
    return temp;
}

void NumberParameter::read()
{
    cin >> mValue;
}

void NumberParameter::write() const
{
    cout << toString();
}

string NumberParameter::toString() const
{
    return boost::lexical_cast<std::string,double>(mValue);
}

std::tr1::shared_ptr< Parameter > NumberParameter::zero() const
{
    static std::tr1::shared_ptr<Parameter> temp(new NumberParameter(0));
    return temp;
}


