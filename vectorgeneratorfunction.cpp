#include "vectorgeneratorfunction.h"
#include "vectorparameter.h"
#include <iostream>
#include <limits>

using namespace std;

VectorGeneratorFunction::VectorGeneratorFunction(): Function()
{

}

VectorGeneratorFunction::VectorGeneratorFunction(tr1::shared_ptr< Function > pFirstArgument): Function()
{
	mArguments.push_back(pFirstArgument);
}


unsigned int VectorGeneratorFunction::priority()
{
	return numeric_limits<unsigned int>::max();
}

void VectorGeneratorFunction::setArgument(unsigned int argumentID, std::tr1::shared_ptr< Function > pFunction)
{
	if (mArguments.size() < argumentID + 1)
		mArguments.resize(argumentID+1);
	mArguments[argumentID] = pFunction;
}

unsigned int VectorGeneratorFunction::getArgumentNumber()
{
	return 0;
}

std::tr1::shared_ptr< Function > VectorGeneratorFunction::clone()
{
	tr1::shared_ptr<VectorGeneratorFunction> ret(new VectorGeneratorFunction);
	for(unsigned i=0;i<mArguments.size();i++)
		ret->setArgument(i,mArguments[i]);
	return ret;
}

std::string VectorGeneratorFunction::toString()
{
	string ret = "(";
	for(unsigned i=0;i<mArguments.size()-1;i++)
		ret += mArguments[i]->toString() + ", ";
	ret += mArguments.back()->toString() + ")";
	return ret;
}

std::tr1::shared_ptr< Function > VectorGeneratorFunction::derivation()
{
	tr1::shared_ptr<VectorGeneratorFunction> ret(new VectorGeneratorFunction);
	for(unsigned i=0;i<mArguments.size();i++)
		ret->setArgument(i,mArguments[i]->derivation());
	return ret;
}

std::tr1::shared_ptr< Function > VectorGeneratorFunction::simplify()
{
	tr1::shared_ptr<VectorGeneratorFunction> ret(new VectorGeneratorFunction);
	for(unsigned i=0;i<mArguments.size();i++)
		ret->setArgument(i,mArguments[i]->simplify());
	return ret;
}

std::tr1::shared_ptr< Parameter > VectorGeneratorFunction::evaluate()
{
	tr1::shared_ptr<VectorParameter> ret(new VectorParameter);
	for(unsigned i=0;i<mArguments.size();i++)
		ret->setAt(i,mArguments[i]->evaluate());
	return ret;
}

