#pragma once

#include "function.h"

class BasicArithmeticOperator : public Function
{
public:
    BasicArithmeticOperator();
	BasicArithmeticOperator(char pOperand,std::tr1::shared_ptr<Function> pLeft = std::tr1::shared_ptr<Function>((Function*)NULL), std::tr1::shared_ptr<Function> pRight = std::tr1::shared_ptr<Function>((Function*)NULL));
    virtual std::tr1::shared_ptr<Function> clone();
    virtual std::string toString();
    virtual std::tr1::shared_ptr<Function> derivation();
    virtual std::tr1::shared_ptr<Function> simplify();
    virtual std::tr1::shared_ptr<Parameter> evaluate();
	virtual unsigned int getArgumentNumber();
	virtual void setArgument(unsigned argumentID, std::tr1::shared_ptr<Function> pFunction);
	virtual unsigned priority(); 
	inline void setLeftFunction(std::tr1::shared_ptr<Function> pFunction)
	{
		mLeftFunction = pFunction;
	}
	inline void setRightFunction(std::tr1::shared_ptr<Function> pFunction)
	{
		mRightFunction = pFunction;
	}
	inline void setOperand(char pOperand)
	{
		mOperand = pOperand;
	}
	
	inline std::tr1::shared_ptr<Function> getLeftFunction()
	{
		return mLeftFunction;
	}
	inline std::tr1::shared_ptr<Function> getRightFunction()
	{
		return mRightFunction;
	}
	inline char getOperand()
	{
		return mOperand;
	}
	
    virtual ~BasicArithmeticOperator();
protected:
	std::tr1::shared_ptr<Function> mLeftFunction,mRightFunction;
	char mOperand;
};


