#include "customvariablefunction.h"
#include <iostream>
#include "constantfunction.h"
#include <limits>
#include "numberparameter.h"

using namespace std;

std::map<std::string, std::tr1::shared_ptr<Parameter> > CustomVariableFunction::gParamaterlist;

CustomVariableFunction::CustomVariableFunction()
{

}

CustomVariableFunction::CustomVariableFunction(std::string pName) : mName(pName)
{

}

CustomVariableFunction::~CustomVariableFunction()
{

}


unsigned int CustomVariableFunction::priority()
{
    return numeric_limits< unsigned int >::max();
}

unsigned int CustomVariableFunction::getArgumentNumber()
{
    return 0;
}

void CustomVariableFunction::setArgument(unsigned int argumentID, tr1::shared_ptr< Function > pFunction)
{

}

std::tr1::shared_ptr< Function > CustomVariableFunction::clone()
{
    return tr1::shared_ptr<Function>(new CustomVariableFunction(*this));
}

std::string CustomVariableFunction::toString()
{
    return mName;
}

std::tr1::shared_ptr< Function > CustomVariableFunction::derivation()
{
    if (gParamaterlist.find(mName) != gParamaterlist.end())
        return tr1::shared_ptr<Function>(new ConstantFunction(gParamaterlist[mName]->zero()));
    else
		return tr1::shared_ptr<Function>(new ConstantFunction(tr1::shared_ptr<Parameter>(new NumberParameter(1))));        
}

std::tr1::shared_ptr< Function > CustomVariableFunction::simplify()
{
    return tr1::shared_ptr<Function>(this);
}

std::tr1::shared_ptr< Parameter > CustomVariableFunction::evaluate()
{
    if (gParamaterlist.find(mName) != gParamaterlist.end())
        return gParamaterlist[mName];
    else
    {
        gParamaterlist[mName] = tr1::shared_ptr<Parameter>(new NumberParameter);
        cout << "Enter value for parameter "<< mName << "\n";
        gParamaterlist[mName]->read();
        return gParamaterlist[mName];
    }
}

void CustomVariableFunction::resetParameters(tr1::shared_ptr< Function > pFunction)
{
    gParamaterlist.clear();
}
