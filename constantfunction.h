#pragma once

#include "function.h"

class ConstantFunction : public Function
{

public:
	ConstantFunction();
	ConstantFunction(std::tr1::shared_ptr< Parameter > pValue);
	virtual ~ConstantFunction();
    virtual unsigned int getArgumentNumber();
	virtual void setArgument(unsigned argumentID, std::tr1::shared_ptr<Function> pFunction);
	virtual std::tr1::shared_ptr< Function > clone();
    virtual std::string toString();
    virtual std::tr1::shared_ptr< Function > derivation();
    virtual std::tr1::shared_ptr< Function > simplify();
    virtual std::tr1::shared_ptr< Parameter > evaluate();
	virtual unsigned priority();
protected:
	std::tr1::shared_ptr<Parameter> mValue;
};

