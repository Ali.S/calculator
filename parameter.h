#pragma once
#include <string>
#include <memory>

class Parameter
{
public:
	Parameter(){};
	virtual ~Parameter(){};
	virtual void read() = 0;
	virtual void write() const = 0;
	virtual std::string toString() const = 0;
	virtual std::tr1::shared_ptr<Parameter> operator +(const Parameter& pRight) const = 0;
	virtual std::tr1::shared_ptr<Parameter> operator -(const Parameter& pRight) const = 0;
	virtual std::tr1::shared_ptr<Parameter> operator *(const Parameter& pRight) const = 0;
	virtual std::tr1::shared_ptr<Parameter> operator /(const Parameter& pRight) const = 0;
	virtual std::tr1::shared_ptr<Parameter> zero() const = 0;
protected:
};