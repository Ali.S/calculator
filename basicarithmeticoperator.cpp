#include "basicarithmeticoperator.h"
#include "numberparameter.h"
#include <iostream>

using namespace std;

BasicArithmeticOperator::BasicArithmeticOperator() : Function(), mLeftFunction((Function*)NULL), mRightFunction((Function*)NULL)
{

}

BasicArithmeticOperator::BasicArithmeticOperator(char pOperand, tr1::shared_ptr< Function > pLeft, tr1::shared_ptr< Function > pRight): Function(), mLeftFunction(pLeft), mRightFunction(pRight), mOperand(pOperand)
{

}

tr1::shared_ptr<Function> BasicArithmeticOperator::clone()
{
    tr1::shared_ptr<BasicArithmeticOperator> ret(new BasicArithmeticOperator);
    ret->mLeftFunction = mLeftFunction->clone();
    ret->mRightFunction = mRightFunction->clone();
    ret->mOperand = mOperand;
    return ret;
}

std::string BasicArithmeticOperator::toString()
{
	string ret;
	if (mOperand == 'n')
		ret += "-";
	if (mLeftFunction->priority() < priority())
		ret += "(" + mLeftFunction->toString()+ ")";
	else
		ret += mLeftFunction->toString();
	
	if (mOperand == 'n')
		return ret;
	ret += mOperand;
	if (mRightFunction->priority() < priority())
		ret += "(" + mRightFunction->toString()+ ")";
	else
		ret += mRightFunction->toString();
	
    return ret;
}

unsigned int BasicArithmeticOperator::getArgumentNumber()
{
	int num = 0;
	if (mLeftFunction == NULL)
		num ++;
	if (mRightFunction == NULL && mOperand != 'n')
		num ++;
    return num;
}

void BasicArithmeticOperator::setArgument(unsigned int argumentID, tr1::shared_ptr< Function > pFunction)
{
    switch (argumentID)
    {
    case 0:
        mLeftFunction = pFunction;
        break;
    case 1:
        mRightFunction = pFunction;
        break;
    }
}

tr1::shared_ptr<Function> BasicArithmeticOperator::derivation()
{
    tr1::shared_ptr<Function> ret;
    switch (mOperand) {
    case '+' :
        ret = tr1::shared_ptr<Function>(new BasicArithmeticOperator('+', mLeftFunction->derivation(), mRightFunction->derivation()));
        break;
    case '-' :
        ret = tr1::shared_ptr<Function>(new BasicArithmeticOperator('-', mLeftFunction->derivation(), mRightFunction->derivation()));
        break;
	case 'n' :
		ret = tr1::shared_ptr<Function>(new BasicArithmeticOperator('n', mLeftFunction->derivation(), tr1::shared_ptr<Function>((Function*)nullptr)));
		break;
	case '*' :
		ret = tr1::shared_ptr<Function>(new BasicArithmeticOperator('+', 
			tr1::shared_ptr<Function>(new BasicArithmeticOperator('*', mLeftFunction->derivation(), mRightFunction)),
			tr1::shared_ptr<Function>(new BasicArithmeticOperator('*', mLeftFunction, mRightFunction->derivation()))
			));
		break;
    case '/' :
		ret = tr1::shared_ptr<Function>(new BasicArithmeticOperator('/', 
			tr1::shared_ptr<Function>(new BasicArithmeticOperator('-', 
				tr1::shared_ptr<Function>(new BasicArithmeticOperator('*', mLeftFunction->derivation(), mRightFunction)),
				tr1::shared_ptr<Function>(new BasicArithmeticOperator('*', mLeftFunction, mRightFunction->derivation()))
				)),
			tr1::shared_ptr<Function>(new BasicArithmeticOperator('*', mRightFunction, mRightFunction))
			));
		break;
    }
    return ret;
}

tr1::shared_ptr<Function> BasicArithmeticOperator::simplify()
{
    return tr1::shared_ptr<Function>(this);
}

tr1::shared_ptr<Parameter> BasicArithmeticOperator::evaluate()
{
    tr1::shared_ptr<Parameter> ret;
    switch (mOperand) {
    case '+' :
        ret = mLeftFunction->evaluate()->operator +(*mRightFunction->evaluate());
        break;
    case 'n' :
        ret = mLeftFunction->evaluate()->operator *(NumberParameter(-1));
        break;
    case '-' :
        ret = mLeftFunction->evaluate()->operator -(*mRightFunction->evaluate());
        break;
    case '*' :
        ret = mLeftFunction->evaluate()->operator *(*mRightFunction->evaluate());
        break;
    case '/' :
        ret = mLeftFunction->evaluate()->operator /(*mRightFunction->evaluate());
        break;
	};
    return ret;
}

unsigned int BasicArithmeticOperator::priority()
{

    if (mOperand == '+' || mOperand == '-')
        return 2;
    if (mOperand == '*' || mOperand == '/')
        return 3;
    if (mOperand == '^')
        return 4;
	if (mOperand == 'n')
		return 5;
    return 0;
}

BasicArithmeticOperator::~BasicArithmeticOperator()
{

}

/*
 * 1+5*6 =>
 * *6+15
 *
 *
 */
