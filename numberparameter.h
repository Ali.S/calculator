#pragma once
#include "parameter.h"


class NumberParameter : public Parameter
{

public:
	NumberParameter();
    NumberParameter(const NumberParameter& pOther);
	NumberParameter(double pValue);
    virtual ~NumberParameter();
    virtual std::tr1::shared_ptr<Parameter> operator/(const Parameter& pRight) const;
    virtual std::tr1::shared_ptr<Parameter> operator*(const Parameter& pRight) const;
    virtual std::tr1::shared_ptr<Parameter> operator-(const Parameter& pRight) const;
    virtual std::tr1::shared_ptr<Parameter> operator+(const Parameter& pRight) const;
    virtual void write() const;
    virtual void read();
	virtual std::string toString()const;
	inline double getValue() const {return mValue;};
	virtual std::tr1::shared_ptr<Parameter> zero() const;
protected:
	double mValue;
};
