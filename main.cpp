#include <iostream>
#include "function.h"
#include "oneparametercustomfunction.h"
#include "math.h"
#define regfunction(X,Y)\
	
Function::getKnownFunctions()[#X] = std::tr1::shared_ptr<Function>(new CustomFunction(#X,X));
using namespace std;

double k(double x,double y)
{
	return x*y+1;
}

int main(int argc, char **argv) {

    //cout << "test\n";
	regfunction(sin);
	regfunction(cos);
	//regfunction(exp);

	Function::getKnownFunctions()["exp"] = std::tr1::shared_ptr<Function>(new CustomFunction("exp",exp));
	regfunction(sqrt);
	regfunction(atan2);
	regfunction(hypot);
	regfunction(sinh);
	regfunction(k);
	//string s = "(hypot(a,b),sinh(a))";
	string s="(exp(x),1)";
    //getline(cin,s);
    tr1::shared_ptr<Function> test = Function::fromString(s);
	cout << "completly parsed" << endl;
	cerr << "\n****************\n\n";
	cerr << test->toString() << "\n";
//	cerr << test->evaluate()->toString() << "\n";
	cerr << test->derivation()->toString() << "\n";
	cerr << test->derivation()->derivation()->toString() << "\n";
	cin.get();
    return 0;
}
