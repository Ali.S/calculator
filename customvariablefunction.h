#pragma once
#include "function.h"


class CustomVariableFunction : public Function
{

public:
	CustomVariableFunction();
    CustomVariableFunction(std::string pName);
    virtual ~CustomVariableFunction();
    virtual unsigned int priority();
    virtual unsigned int getArgumentNumber();
	virtual void setArgument(unsigned argumentID, std::tr1::shared_ptr<Function> pFunction);
    virtual std::tr1::shared_ptr< Function > clone();
    virtual std::string toString();
    virtual std::tr1::shared_ptr< Function > derivation();
    virtual std::tr1::shared_ptr< Function > simplify();
    virtual std::tr1::shared_ptr< Parameter > evaluate();
	static void resetParameters(std::tr1::shared_ptr<Function> pFunction);
protected:
private:
	std::string mName;
	static std::map<std::string, std::tr1::shared_ptr<Parameter> > gParamaterlist;
};
