#include "customfunction.h"
#include "vectorgeneratorfunction.h"
#include <iostream>
#include <limits>
#include "vectorparameter.h"
#include "numberparameter.h"

using namespace std;

CustomFunction::CustomFunction() : Function()
{
	//mArguments = std::tr1::shared_ptr<Function>((Function*) NULL);
}

CustomFunction::CustomFunction(string pName): mName(pName)
{

}

const std::string& CustomFunction::getName() const
{
	return mName;
}

void CustomFunction::setName(string pName)
{
	mName = pName;
}


unsigned int CustomFunction::priority()
{
	return numeric_limits< int >::max() - 1;
}

void CustomFunction::setArgument(unsigned int argumentID, std::tr1::shared_ptr< Function > pFunction)
{
	mArguments[argumentID] = pFunction;
}

unsigned int CustomFunction::getArgumentNumber()
{
	int k=0;
	for(unsigned i=0;i<mArguments.size();i++)
		if(mArguments[i] == NULL)
			k++;
	return k;
}

tr1::shared_ptr<Function> CustomFunction::clone()
{
	CustomFunction* temp = new CustomFunction;
	temp->mFunction = mFunction;
	temp->mDerivation = mDerivation;
	temp->mArguments.resize(mArguments.size());
	for(unsigned i=0;i<mArguments.size ();i++)
		if(mArguments[i] != nullptr)
			temp->mArguments[i] = mArguments[i]->clone();
		else
			temp->mArguments[i] = std::tr1::shared_ptr<Function>((Function*)NULL);
	temp->mName = mName;
	return std::tr1::shared_ptr<Function>(temp);
}

tr1::shared_ptr<Function> CustomFunction::simplify()
{
	CustomFunction* temp = new CustomFunction;
	temp->mFunction = mFunction;
	temp->mDerivation = mDerivation;
	temp->mArguments.resize(mArguments.size());
	for(unsigned i=0;i<mArguments.size ();i++)
		if(mArguments[i] != nullptr)
			temp->mArguments[i] = mArguments[i]->simplify();
		else
			temp->mArguments[i] = std::tr1::shared_ptr<Function>((Function*)NULL);
	temp->mName = mName;
	return std::tr1::shared_ptr<Function>(temp);
}

tr1::shared_ptr<Function> CustomFunction::derivation()
{
	if (mDerivation == nullptr)
		return nullptr;
	tr1::shared_ptr<CustomFunction> temp = mDerivation->clone().get();
	temp->mFunction = mFunction;
	temp->mDerivation = mDerivation;
	temp->mArguments.resize(mArguments.size());
	for(unsigned i=0;i<mArguments.size ();i++)
		if(mArguments[i] != nullptr)
			temp->mArguments[i] = mArguments[i]->clone();
		else
			temp->mArguments[i] = std::tr1::shared_ptr<Function>((Function*)NULL);
	temp->mName = mName;
	return std::tr1::shared_ptr<Function>(temp);
}

std::string CustomFunction::toString()
{
	string res = mName;
	res += "(" ;
	if (mArguments.size() > 0)
	{
		if (mArguments[0])
			res += mArguments[0]->toString();
		else
			res += "<UNKNOWN>";
		for(unsigned i=1;i<mArguments.size();i++)
			if (mArguments[i])
				res += ", " + mArguments[i]->toString();
			else
				res += ", <UNKNOWN>";
	}
	res += ")";
	return res;
		
}

double evaluationHelper(double* pArguments, unsigned pNumDoubles, double(*pFunction)(...)) 
{
	double res;
#ifdef WIN32
	double*t= &res;
	pArguments+= (pNumDoubles-1);
	__asm push ebp
	__asm push t
	__asm mov eax, pArguments
	__asm mov ebx, pNumDoubles
	__asm mov ecx, pFunction
	__asm mov ebp, esp
		// arguement passing loop
	__asm evaluation_Helper_label_loop:
	__asm sub esp, 8
	__asm fld qword ptr [eax]
	__asm fstp qword ptr [esp]
	__asm sub ebx, 1
	__asm sub eax, 8
	__asm and ebx, ebx
	__asm jnz evaluation_Helper_label_loop
		// call function
	__asm call ecx
		// collecting result
	__asm mov esp, ebp
	__asm pop edx
	__asm fstp qword ptr [edx]
	__asm pop ebp
		
#else
	asm(
		// set up the frame pointer 
		"push ebp"             "\n\t"
		"push %3"              "\n\t"
		"mov ebp, esp"         "\n\t"
		// arguement passing loop
		"evaluation_Helper_label_loop:"          "\n\t"
		"sub esp, 8"           "\n\t"
		"fld qword ptr [%1]"   "\n\t"
		"fstp qword ptr [esp]" "\n\t"
		"sub %0, 1"            "\n\t"
		"sub %1, 8"            "\n\t"
		"test %0, %0"          "\n\t"
		"jnz evaluation_Helper_label_loop"       "\n\t"
		// call function
		"call %2"              "\n\t"
		// collecting result
		"mov esp, ebp"         "\n\t"
		"pop %3"               "\n\t"
		"fstp qword ptr [%3]"  "\n\t"
		"pop ebp"              "\n\t"
		: // no output 
		:"r"(pNumDoubles), "r"(pArguments + (pNumDoubles-1)), "r"(pFunction),"b"(&res) // input
		);
#endif
	return res;
}

tr1::shared_ptr<Parameter> CustomFunction::evaluate()
{
	unsigned numDoubles = mArguments.size();
	if (mArguments.size() == 0)
		return tr1::shared_ptr<Parameter>(new NumberParameter(mFunction()));
	vector<tr1::shared_ptr<Parameter> > arguments(numDoubles);
	for(unsigned i=0;i<numDoubles;i++)
		arguments [i] = mArguments[i]->evaluate();
	return tryEvaluate(arguments);
}

std::tr1::shared_ptr<Parameter> CustomFunction::tryEvaluate(const std::vector<std::tr1::shared_ptr<Parameter> >& pArguments)
{
	int flag;
	Parameter* p = &*pArguments[0];
	if (dynamic_cast<NumberParameter*>(p))
		flag = -1;
	else
		if (dynamic_cast<VectorParameter*>(p))
			flag = dynamic_cast<VectorParameter*>(p)->getSize();
		else
			flag = 0;
	for(unsigned i=1;i<pArguments.size();i++)
	{
		p = &*pArguments[i];
		if (dynamic_cast<NumberParameter*>(p) && flag != -1)
			flag = 0;
		if (dynamic_cast<VectorParameter*>(p) && flag != (int)dynamic_cast<VectorParameter*>(p)->getSize())
			flag = 0;
	}
	tr1::shared_ptr<Parameter> res;
	if(flag == -1)
	{
		
		double* toPass = new double[pArguments.size()]; 
		for(unsigned i=0;i<pArguments.size();i++)
			toPass[i] = static_cast<NumberParameter*>(&*pArguments[i])->getValue();
		res = tr1::shared_ptr<Parameter>(new NumberParameter(evaluationHelper(toPass,pArguments.size(),mFunction)));
		delete []toPass;
	}
	if(flag > 0)
	{
		tr1::shared_ptr<VectorParameter> tempres(new VectorParameter);
		vector<tr1::shared_ptr<Parameter> > toPass(pArguments.size());
		for(int j=0;j<flag;j++)
		{
			for(unsigned i=0;i<pArguments.size();i++)
				toPass[i] = static_cast<VectorParameter*>(&*pArguments[i])->getAt(j);
			tempres->setAt(j,tryEvaluate(toPass));
		}
		res = tempres;
	}
	return res;
}

void CustomFunction::setDerivation(tr1::shared_ptr<CustomFunction> derivation)
{
	mDerivation = derivation;
}

CustomFunction::CustomFunction(std::string pName,double (*pFunction)(double))
{
	mName = pName;
	mFunction = reinterpret_cast<double(*)(...)>(pFunction);
	mDerivation = nullptr;
	mArguments.resize(1);
}
void CustomFunction::setFunction(double (*pFunction)(double))
{
	mFunction = reinterpret_cast<double(*)(...)>(pFunction);
	mDerivation = nullptr;
	mArguments.clear();
	mArguments.resize(1);
}

// two parameters 
CustomFunction::CustomFunction(std::string pName,double (*pFunction)(double,double))
{
	mName = pName;
	mFunction = reinterpret_cast<double(*)(...)>(pFunction);
	mDerivation = nullptr;
	mArguments.resize(2);
}
void CustomFunction::setFunction(double (*pFunction)(double,double))
{
	mFunction = reinterpret_cast<double(*)(...)>(pFunction);
	mDerivation = nullptr;
	mArguments.clear();
	mArguments.resize(2);
}

// three parameters
CustomFunction::CustomFunction(std::string pName,double (*pFunction)(double,double,double))
{
	mName = pName;
	mFunction = reinterpret_cast<double(*)(...)>(pFunction);
	mDerivation = nullptr;
	mArguments.resize(3);
}
void CustomFunction::setFunction(double (*pFunction)(double,double,double))
{
	mFunction = reinterpret_cast<double(*)(...)>(pFunction);
	mDerivation = nullptr;
	mArguments.clear();
	mArguments.resize(1);
}
