#pragma once
#include "parameter.h"
#include <string>
#include <map>
#include <memory>
#include <stack>
#include <vector>

class Function
{
public:
	Function();
	virtual ~Function();
	virtual std::tr1::shared_ptr<Parameter> evaluate() = 0;
	virtual std::tr1::shared_ptr<Function> simplify() = 0;
	virtual std::tr1::shared_ptr<Function> derivation() = 0;
	virtual std::string toString() = 0;
	virtual std::tr1::shared_ptr<Function> clone() = 0;
	virtual unsigned getArgumentNumber() = 0;
	virtual void setArgument(unsigned argumentID, std::tr1::shared_ptr< Function > pFunction) = 0;
	virtual unsigned priority() = 0;
	static std::tr1::shared_ptr<Function> fromString(const std::string& data);
	inline static std::map<std::string, std::tr1::shared_ptr<Function> >& getKnownFunctions() 
	{
		if (!mKnownFunctions)
			mKnownFunctions = new std::map<std::string, std::tr1::shared_ptr<Function> >;
		return *mKnownFunctions;
	};
private:
	enum ArgumentType
	{
		argNumber,
		argVector,
		argMatris,
		argUnknown
	};
	static std::tr1::shared_ptr<Function> localFromString(const std::string& data,unsigned& pos);
	static std::tr1::shared_ptr<Function> localVectorGenerator(const std::string& data,unsigned& pos);
	static std::tr1::shared_ptr<Function> localCustomFunctionGenerator(const std::string& data, unsigned int &pos,std::tr1::shared_ptr<Function> target);
	static void localInsert(std::stack< std::pair< std::tr1::shared_ptr< Function >, unsigned int > >& pStack, std::vector< std::tr1::shared_ptr< Function > >& pVector, std::tr1::shared_ptr< Function > pFunction);
	static void localInsert(std::stack< std::pair< std::tr1::shared_ptr< Function >, unsigned int > >& pStack, std::vector< std::tr1::shared_ptr< Function > >& pVector, std::tr1::shared_ptr< Function > pFunction, unsigned int pOverridePriority);
	static void localParseString(const std::string& data,unsigned& pos,std::string& target,ArgumentType& dataType);
	static void localSkipWhiteSpace(const std::string& data,unsigned& pos);
    static void createFunctionTree(std::vector< std::tr1::shared_ptr< Function > >& pVector);
	static std::map<std::string, std::tr1::shared_ptr<Function> > *mKnownFunctions;
};