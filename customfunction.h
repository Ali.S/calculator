#pragma once
#include "function.h"


class CustomFunction : public Function
{
public:
    CustomFunction();
	CustomFunction(std::string pName);
	void setName(std::string pName);
	const std::string& getName() const;
    virtual unsigned int priority();
    virtual void setArgument(unsigned int argumentID, std::tr1::shared_ptr< Function > pFunction);
    virtual unsigned int getArgumentNumber();
	virtual std::tr1::shared_ptr<Parameter> evaluate();
	virtual std::tr1::shared_ptr<Function> simplify();
	virtual std::tr1::shared_ptr<Function> derivation();
	virtual std::string toString();
	virtual std::tr1::shared_ptr<Function> clone();

	void setDerivation(std::tr1::shared_ptr<CustomFunction> derivation);
	// one parameter 
	CustomFunction(std::string pName,double (*pFunction)(double));
    virtual void setFunction(double (*pFunction)(double));
    // two parameters 
	CustomFunction(std::string pName,double (*pFunction)(double,double));
    virtual void setFunction(double (*pFunction)(double,double));
    // three parameters
	CustomFunction(std::string pName,double (*pFunction)(double,double,double));
    virtual void setFunction(double (*pFunction)(double,double,double));
    
protected:
	virtual std::tr1::shared_ptr<Parameter> tryEvaluate(const std::vector<std::tr1::shared_ptr<Parameter> >& pArguments);
	double (*mFunction)(...);
	std::tr1::shared_ptr<CustomFunction> mDerivation;
	std::string mName;
	std::vector<std::tr1::shared_ptr<Function> > mArguments;
};
