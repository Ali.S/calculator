#include "vectorparameter.h"
#include "numberparameter.h"
#include <iostream>
#include <sstream>

using namespace std;

std::tr1::shared_ptr< Parameter > VectorParameter::zero() const
{
	tr1::shared_ptr<VectorParameter> ret = tr1::shared_ptr<VectorParameter>(new VectorParameter);
	return ret;
}

std::tr1::shared_ptr< Parameter > VectorParameter::operator/(const Parameter& pRight) const
{
	std::tr1::shared_ptr<Parameter> ret;
    if (dynamic_cast<const NumberParameter*>(&pRight))
	{
		std::tr1::shared_ptr<VectorParameter> temp(new VectorParameter);
		for(unsigned i = 0;i<mValues.size();i++)
			temp->setAt(i, (*mValues[i]) / pRight);
		ret = temp;
	}
	return ret;
}

std::tr1::shared_ptr< Parameter > VectorParameter::operator*(const Parameter& pRight) const
{
	std::tr1::shared_ptr<Parameter> ret;
    if (dynamic_cast<const NumberParameter*>(&pRight))
	{
		std::tr1::shared_ptr<VectorParameter> temp;
        temp = std::tr1::shared_ptr<VectorParameter>(new VectorParameter);
		for(unsigned i = 0;i<mValues.size();i++)
			temp->setAt(i,(*mValues[i]) * pRight);
		ret = temp;
	}
	
	/*if (const VectorParameter* right = dynamic_cast<const VectorParameter*>(&pRight))
		if(right->mValues.size() == mValues.size())
		{			
			float res = 0;
			for(unsigned i = 0;i<mValues.size();i++)
				if (mValues[i]
				res += ((*mValues[i]) * *right)->getValue();
			ret = new NumberParameter(res);
		}*/
    return ret;

}

std::tr1::shared_ptr< Parameter > VectorParameter::operator-(const Parameter& pRight) const
{
	std::tr1::shared_ptr<Parameter> ret;
    if (const VectorParameter* right = dynamic_cast<const VectorParameter*>(&pRight))
		if(right->mValues.size() == mValues.size())
		{
			std::tr1::shared_ptr<VectorParameter> temp;
			temp = std::tr1::shared_ptr<VectorParameter>(new VectorParameter);
			for(unsigned i = 0;i<mValues.size();i++)
				temp->setAt(i, (*mValues[i]) - *right->mValues[i]);
			ret = temp;
		}
	return ret;
}

std::tr1::shared_ptr< Parameter > VectorParameter::operator+(const Parameter& pRight) const
{
	std::tr1::shared_ptr<Parameter> ret;
    if (const VectorParameter* right = dynamic_cast<const VectorParameter*>(&pRight))
		if(right->mValues.size() == mValues.size())
		{
			std::tr1::shared_ptr<VectorParameter> temp;
			temp = static_cast<std::tr1::shared_ptr<VectorParameter> >(new VectorParameter);
			for(unsigned i = 0;i<mValues.size();i++)
				temp->setAt(i, (*mValues[i]) + *right->mValues[i]);
			ret = temp;
		}
	return ret;
}

std::string VectorParameter::toString() const
{
	stringstream buffer;
	buffer << "(";
	for(unsigned i=0;i<mValues.size()-1;i++)
		buffer << mValues[i]->toString() << ", ";
	buffer << mValues.back()->toString();
	buffer << ")";
	return buffer.str();
}

void VectorParameter::write() const
{
	cout << "(";
	for(unsigned i=0;i<mValues.size()-1;i++)
		cout << mValues[i]->toString() << ", ";
	cout << mValues.back()->toString();
	cout << ")";
}

void VectorParameter::read()
{
	for(unsigned i=0;i<mValues.size();i++)
		mValues[i]->read();
}

