#include "oneparametercustomfunction.h"
#include "basicarithmeticoperator.h"
#include "numberparameter.h"
#include <iostream>

OneParameterCustomFunction::OneParameterCustomFunction() : CustomFunction()
{
	mArguments.resize(0);
	mFunction = NULL;
	mDerivation = NULL;
}

OneParameterCustomFunction::OneParameterCustomFunction(std::string pName, double(*pFunction)(double) , double(*pDerivation)(double)): CustomFunction(pName)
{
	mArguments.resize(0);
	mFunction = pFunction;
	mDerivation = pDerivation;
}

OneParameterCustomFunction::~OneParameterCustomFunction()
{
	//while initializing
	mFunction = sin;
	//later in code
	double (*generalized)(...) = mFunction;
	for(i=0;i<args.size();i++)
		pusharg(args[i]);
	call(generalized);
}

std::tr1::shared_ptr< Function > OneParameterCustomFunction::clone()
{
	OneParameterCustomFunction * temp = new OneParameterCustomFunction;
	temp->mFunction = mFunction;
	temp->mDerivation = mDerivation;
	temp->mArguments.resize(mArguments.size());
	if (mArguments)
		temp->mArguments = mArguments->clone();
	else
		temp->mArguments = std::tr1::shared_ptr<Function>((Function*)NULL);
	temp->mName = mName;
	return std::tr1::shared_ptr<Function>(temp);
}

void OneParameterCustomFunction::setFunction(double (*pFunction)(double),double (*pDerivation)(double) )
{
	mFunction  = pFunction;
	mDerivation = pDerivation;
}

std::string OneParameterCustomFunction::toString()
{
	std::cout << "seting for " << mName << " : "<< mArguments->toString() << std::endl;
	return mName + mArguments->toString();
}

std::tr1::shared_ptr< Function > OneParameterCustomFunction::derivation()
{
	if(mDerivation)
	{
		OneParameterCustomFunction * temp = new OneParameterCustomFunction;
		temp->mFunction = mDerivation;
		temp->mDerivation = NULL;
		temp->mArguments = mArguments->clone();
		return std::tr1::shared_ptr<Function>(new BasicArithmeticOperator('*',mArguments->derivation(),std::tr1::shared_ptr<Function>(temp)));
	}
	else
		return std::tr1::shared_ptr<Function>((Function*)NULL);
}

std::tr1::shared_ptr< Function > OneParameterCustomFunction::simplify()
{
	OneParameterCustomFunction * temp = new OneParameterCustomFunction;
	temp->mFunction = mFunction;
	temp->mDerivation = mDerivation;
	temp->mArguments = mArguments->simplify();
	return std::tr1::shared_ptr<Function>(temp);
}

std::tr1::shared_ptr< Parameter > OneParameterCustomFunction::evaluate()
{
	std::tr1::shared_ptr<Parameter> p = mArguments->evaluate();
	if (dynamic_cast<NumberParameter*> (&(*p)))
		return std::tr1::shared_ptr<Parameter>(new NumberParameter(mFunction(dynamic_cast<NumberParameter*>(&(*p))->getValue())));
	else
		return std::tr1::shared_ptr<Parameter>((Parameter*)NULL);
}

